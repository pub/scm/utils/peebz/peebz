b4>=0.12.2,<1.0
Pygments>=2.14.0,<3.0
requests>=2.28
sqlalchemy>=1.4,<2.0
tomli>=2.0; python_version < '3.11'
